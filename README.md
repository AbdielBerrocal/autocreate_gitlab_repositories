This is a simple script that creates a new project in gitlab remotely.

-------
<h1><b>Dependencies</b></h1>

1. cURL already installed in your machine (this is just if you don't).
2. Internet access (yes, it sounds obviously).

-------
<h1><b>STEPS</b></h1>

1. You need to create your Access Token (located in your GitLab settings tab).
2. Save it in a secure place.
3. And any time you want to create a new project remotely, just execute the script and it will tell you what to do next.

-------
<b>If you found my script useful, please let me know :D</b>

<b>Follow me on twitter:</b> <a href="https://twitter.com/FrostAbdiel">@FrostAbdiel</a>